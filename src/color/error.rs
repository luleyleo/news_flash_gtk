use thiserror::Error;

#[derive(Error, Debug)]
pub enum ColorError {
    #[error("Color string containing illegal character: '{0}'")]
    IllegalCharacter(char),
    #[error("Error converting rgb color to hsla: '{0}'")]
    RgbToHsla(String),
    #[error("Error converting hsla color to rgb: '{0}'")]
    HslaToRgb(String),
    #[error("Error parsing color string: '{0}'")]
    Parse(String),
}
