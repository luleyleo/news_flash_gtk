window.addEventListener('scroll', on_scroll);
on_scroll();

function on_scroll() {
    var divTop = document.getElementById('overshoot-overlay-top');
    var divBottom = document.getElementById('overshoot-overlay-bottom');

    if (window.scrollY > 0) {
        divTop.classList.add("overshoot-overlay-top");
    } else {
        divTop.classList.remove("overshoot-overlay-top");
    }

    var limit = Math.max(
        document.body.scrollHeight,
        document.body.offsetHeight,
        document.documentElement.clientHeight,
        document.documentElement.scrollHeight,
        document.documentElement.offsetHeight);
    var max_scroll = limit - window.innerHeight;

    if (window.scrollY >= max_scroll) {
        divBottom.classList.remove("overshoot-overlay-bottom");
    } else {
        divBottom.classList.add("overshoot-overlay-bottom");
    }
}